TO DO  bingo-cards  Jeff Davies 1 jan 2003
===========================================

Done:
-----

basic command line C version (POSIX)
basic java command line version
Borland CBuilder 3 (the free copy given away on magazine covers) GUI version for Win32.

To Do:
------

0. Make a bunch of super-high quality pictures for a picture bingo set. (reduced photos of everyday objects)
1. Word Bingo (** DONE FOR JAVA **)  (** NOW DONE IN WIN32 C++ VERSION **)

2. GUI version (work in progress in C for Apple Carbon API, later Java GUI with Forte4j).
3. Compiled version with GUI for Linux, Mac OSX, Mac OS9(?)
(** NATIVE COMPILED Win32 VERSION compiled with Borland C++ Builder 3**) 

4. Perhaps a way to generate a whole book in PDF format for printing, specifying multiple
picture bingo card picture sets. (maybe using FOP?)
5. *Maybe well before some of the above options* - Document how to use the tool, so it can be used
by magazine publishing graphic artists and suchlike.
** Hopefully the GUI version for win32 and later apple mac will do this.

6. try a cgi-bin version? (**DONE**)
7. make a servlet version maybe...

Jeff Davies

IDEAS FOR BINGO TYPES:

0. Foreign language number and object name bingo. (nice and educational)
And kids bingo for learning numbers, letters, words.

1. break the ice bingo - scan in photos of people in the department. Call names to be circled.
Good for playing with new arrivals, so the new arrival gets to learn the names of other staff fast.

2. Technical terms for items - like sailing equipment or scuba equipment. Bingo could be used for newbies
to rapidly come up to speed.

3. Armed forces plane silhouette bingo.. 

4. Naughty bingo - think up our own games naughty people.


