//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------
USEFORM("Main.cpp", MainForm);
USEFORM("ChildWin.cpp", bingoGame);
USERES("bingocards.res");
USEFORM("about.cpp", AboutBox);

/*
  Bingo Card Creator version 1.01.
Copyright (C) 2003 Jeff Davies  (jeff@llandre.freeserve.co.uk)
address: Pencoed, Cnwch Coch, Trawscoed, Aberystwyth, Ceredigion, SY23 4RR, UK.

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

*/



//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Application->Initialize();
	Application->Title = "bingo-cards";
        Application->CreateForm(__classid(TMainForm), &MainForm);
        Application->CreateForm(__classid(TAboutBox), &AboutBox);
        Application->Run();

	return 0;
}
//---------------------------------------------------------------------
