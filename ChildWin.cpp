//---------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <registry.hpp>
#include <process.h>
#pragma hdrstop

#include "ChildWin.h"
//---------------------------------------------------------------------
#pragma resource "*.dfm"
//---------------------------------------------------------------------

/*
  Bingo Card Creator version 1.01.
Copyright (C) 2003 Jeff Davies  (jeff@llandre.freeserve.co.uk)
address: Pencoed, Cnwch Coch, Trawscoed, Aberystwyth, Ceredigion, SY23 4RR, UK.

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

*/


__fastcall TbingoGame::TbingoGame(TComponent *Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------
void __fastcall TbingoGame::FormClose(TObject *Sender, TCloseAction &Action)
{
	Action = caFree;
}
//---------------------------------------------------------------------









//---------------------------------------------------------------------------


void __fastcall TbingoGame::pictureBingoClick(TObject *Sender)
{
    pictureOrWordToAdd->Enabled=false;
    WordOrPictureLabel->Enabled=false;
    pictureFileButton->Enabled=true;
    addListItem->Enabled=false;
    removeListItem->Enabled=true;
    ItemListLabel->Enabled=true;
    ItemList->Enabled=true;
    ItemCountLabel->Enabled=true;
    ItemCount->Enabled=true;
    numberBingoMaxNumberLabel->Enabled=false;
    numberBingoMaximumNumber->Enabled=false;
    numberBingoMaxUpDown->Enabled=false;
    RemoveAll->Enabled=true;
}
//---------------------------------------------------------------------------






void __fastcall TbingoGame::numberBingoClick(TObject *Sender)
{
    pictureOrWordToAdd->Enabled=false;
    WordOrPictureLabel->Enabled=false;
    pictureFileButton->Enabled=false;
    addListItem->Enabled=false;
    removeListItem->Enabled=false;
    ItemListLabel->Enabled=false;
    ItemList->Enabled=false;
    ItemCountLabel->Enabled=false;
    ItemCount->Enabled=false;
    numberBingoMaxNumberLabel->Enabled=true;
    numberBingoMaximumNumber->Enabled=true;
    numberBingoMaxUpDown->Enabled=true;
    RemoveAll->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TbingoGame::wordBingoClick(TObject *Sender)
{
    pictureOrWordToAdd->Enabled=true;
    WordOrPictureLabel->Enabled=true;
    pictureFileButton->Enabled=false;
    addListItem->Enabled=true;
    removeListItem->Enabled=true;
    ItemListLabel->Enabled=true;
    ItemList->Enabled=true;
    ItemCountLabel->Enabled=true;
    ItemCount->Enabled=true;
    numberBingoMaxNumberLabel->Enabled=false;
    numberBingoMaximumNumber->Enabled=false;
    numberBingoMaxUpDown->Enabled=false;
    RemoveAll->Enabled=true;
}
//---------------------------------------------------------------------------



void __fastcall TbingoGame::outputFileButtonClick(TObject *Sender)
{
	if(outputfileOpenDialog->Execute())
	outputFilename->Text = outputfileOpenDialog->FileName;
}
//---------------------------------------------------------------------------


void __fastcall TbingoGame::pictureFileButtonClick(TObject *Sender)
{
	if(picturefileOpenDialog->Execute()) {
    	//pictureOrWordToAdd->Text = picturefileOpenDialog->FileName;
        ItemList->Items->AddStrings( picturefileOpenDialog->Files );
        ItemCount->Text=ItemList->Items->Count;
    };
}
//---------------------------------------------------------------------------


void __fastcall TbingoGame::addListItemClick(TObject *Sender)
{
  int itemnum,itemfound;
  if (pictureOrWordToAdd->Text=="")
    return;
  itemfound=-1;
  for (itemnum=0;itemnum<ItemList->Items->Count;itemnum++) {
    if (ItemList->Selected[itemnum]) {
      itemfound=itemnum;
      break;
    };
  };

  if (itemfound==-1) {
    ItemList->Items->Append( pictureOrWordToAdd->Text);
  } else {
    ItemList->Items->Insert(itemfound, pictureOrWordToAdd->Text);
  };
  ItemCount->Text=ItemList->Items->Count;
  pictureOrWordToAdd->Text="";

}
//---------------------------------------------------------------------------

void __fastcall TbingoGame::removeListItemClick(TObject *Sender)
{
  int itemnum,itemfound;
  itemfound=-1;
  for (itemnum=0;itemnum<ItemList->Items->Count;itemnum++) {
    if (ItemList->Selected[itemnum]) {
      itemfound=itemnum;
      break;
    };
  };
  if (itemfound==-1) return; //not found

  //originally the behaviour of this was to move the erased string to the word
  //input field. This is not really sensible. 'Erase' should be erase!
  //   pictureOrWordToAdd->Text=ItemList->Items->Strings[itemfound];
  ItemList->Items->Delete(itemfound );
  ItemCount->Text=ItemList->Items->Count;

}
//---------------------------------------------------------------------------

void __fastcall TbingoGame::removeAllListItemsClick(TObject *Sender)
{
  int itemnum;
  int count=ItemList->Items->Count;
  for (itemnum=0;itemnum<count;itemnum++) {
   ItemList->Items->Delete(0 );
  };

   ItemCount->Text=ItemList->Items->Count;

}









void __fastcall TbingoGame::makeBingoCardsOutputFileClick(TObject *Sender)
{
  int sortrow=0;
  int xml=0;
  TStringList *string_out=new TStringList;

    int selected=0;
    if (numberBingo->Checked==true) selected=1; //number, picture, word
    if (pictureBingo->Checked==true) selected=2;
    if (wordBingo->Checked==true) selected=3;

  AnsiString as_numberOfCalledItemsPerRow,as_numberOfCallingSequences;
  as_numberOfCalledItemsPerRow=numberOfCalledItemsPerRow->Text;
  as_numberOfCallingSequences=numberOfCallingSequences->Text;
  int i_numberOfCalledItemsPerRow=as_numberOfCalledItemsPerRow.ToInt();
  int i_numberOfCallingSequences=as_numberOfCallingSequences.ToInt();

  AnsiString as_columnsPerBingoCard=columnsPerBingoCard->Text;
  AnsiString as_rowsPerBingoCard=rowsPerBingoCard->Text;
  AnsiString as_columnsOfCardsInFile=columnsOfCardsInFile->Text;
  AnsiString as_rowsOfCardsInFile=rowsOfCardsInFile->Text;
  int i_columnsPerBingoCard=as_columnsPerBingoCard.ToInt();
  int i_rowsPerBingoCard=as_rowsPerBingoCard.ToInt();
  int i_columnsOfCardsInFile=as_columnsOfCardsInFile.ToInt();
  int i_rowsOfCardsInFile=as_rowsOfCardsInFile.ToInt();
  int number_of_items_in_list;
  if (selected==1) {
     //number bingo
     AnsiString as_number_of_items_in_list=numberBingoMaximumNumber->Text;
     number_of_items_in_list=as_number_of_items_in_list.ToInt();
  } else {
     //picture or word
     number_of_items_in_list=ItemList->Items->Count;
  };

  bingofunc (
    sortrow, //not implemented yet TODO IMPLEMENT
    xml, //[0,1]
    selected, //[1,2,3] number, picture, word
    i_columnsPerBingoCard,
    i_rowsPerBingoCard,
    i_columnsOfCardsInFile,
    i_rowsOfCardsInFile,
  number_of_items_in_list,
  i_numberOfCalledItemsPerRow,
  i_numberOfCallingSequences,
  "40", //graphics x size   TODO IMPLEMENT ADDITIONAL DIALOG ENTRIES FOR THIS
  "40", //graphics_y_size
  string_out,
  ItemList->Items);

  string_out->SaveToFile(outputFilename->Text);
  delete string_out;

  Application->MessageBox("File made okay","Bingo Card Creator",IDOK);

};

//---------------------------------------------------------------------------



void __fastcall TbingoGame::printcard(
      int xml,
      int sortrow,
      int selected,
      int num_of_cols_per_card_x,
      int num_of_rows_per_card_y,
      char *graphics_x_size,
      char *graphics_y_size,
      int num_of_choices,
      TStrings *string_out,
      TStrings *graphicsname
    )

{



	int card[300];
	int x,y,i,rn;
	int done[300];
	int lfalse=0;
	int ltrue=1;

	//clean "done" array ready for use
	for (i=0;i<num_of_choices;i++) {
		done[i]=lfalse;
	};

	//fill in numbers
    for (y=0;y<num_of_cols_per_card_x;y++) {
       	for (x=0;x<num_of_rows_per_card_y;x++) {

			rn=rand()%num_of_choices;
			while (done[rn]==ltrue) {
				rn=rand()% num_of_choices;
			};
			card[(x*num_of_cols_per_card_x)+y]=rn;
			done[rn]=ltrue;
		};
	};

	//sort rows


	//print numbers
	if (xml==0) {
	  string_out->Add("<table border=\"1\">");
	} else {
	  string_out->Add("<card>\n");
	};

	for (x=0;x<num_of_rows_per_card_y;x++) {
	  if (xml==0) {
	    string_out->Add("<tr>\n");
	  } else {
	    string_out->Add("<row>\n");
	  };

	  for (y=0;y<num_of_cols_per_card_x;y++) {
	    if (xml==0) {
	      string_out->Add("<td>\n");
	    } else {
	      string_out->Add("<column>\n");
	    };

       //number
      if (selected==1) {
	string_out->Add("<td>");
    string_out->Add(card[(x*num_of_cols_per_card_x)+y]);
    string_out->Add("</td>");
      } else if (selected==2) {
      //picture
	string_out->Add("<td><img height=\"");
    string_out->Add(graphics_y_size);
    string_out->Add("\" width=\"");
    string_out->Add(graphics_x_size);
    string_out->Add("\" src=\"");
    string_out->Add(graphicsname->Strings[card[(x*num_of_cols_per_card_x)+y]]);
    string_out->Add("\"></td>");
      } else {
      //word
	string_out->Add("<td>");
    string_out->Add(graphicsname->Strings[card[(x*num_of_cols_per_card_x)+y]]);
    string_out->Add("</td>");
      };


	    if (xml==0) {
	      string_out->Add("</td>\n");
	    } else {
	      string_out->Add("</column>\n");
	    };

	  };

	  if (xml==0) {
	    string_out->Add("</tr>");
	  } else {
	    string_out->Add("</row>\n");
	  };

	};

	if (xml==0) {
	  string_out->Add("</table>");
	} else {
	  string_out->Add("</card>\n");
	};

};

void __fastcall TbingoGame::printheader (int xml, TStrings *string_out) {

  if (xml==0) {
    string_out->Add("<html><head></head><body>");
  } else {
    string_out->Add("<bingogames>");
  };

};

void __fastcall TbingoGame::printfooter (int xml, TStrings *string_out) {

  if (xml==0) {
    string_out->Add("</body></html>");
  } else {
    string_out->Add("</bingogames>");
  };

};


void __fastcall TbingoGame::printrow(
              int xml,
              int sortrow,
              int num_of_cards_x,
              int num_of_cards_y,
              int num_of_cols_per_card_x,
              int num_of_rows_per_card_y,
              int selected,
              char *graphics_x_size,
              char *graphics_y_size,
              int num_of_choices,
              TStrings *string_out,
              TStrings *graphicsname
              )
{

  int col;

  if (xml==0) {
    string_out->Add("<table><tr>");
  };

  for (col=0;col<num_of_cards_x;col++) {
    if (xml==0) {
      string_out->Add("<td>");
    };

    printcard(
      xml,
      sortrow,
      selected,
      num_of_cols_per_card_x,
      num_of_rows_per_card_y,
      graphics_x_size,
      graphics_y_size,
      num_of_choices,
      string_out,
      graphicsname
    );

    if (xml==0) {
      string_out->Add("</td><td>+</td>");
    };
  };

  if (xml==0) {
    string_out->Add("</tr></table>");
  };

}

void __fastcall TbingoGame::printrowsep (int xml, TStrings *string_out) {
  if (xml==0) {
    string_out->Add("<p>");
  };
};

void __fastcall TbingoGame::printcallingseq(
         int xml,
         int selected,
    	 char *graphic_xsize,
         char *graphic_ysize,
         int callingseq_length,
         int callingseq_numbers_per_row,
         TStrings *string_out,
         TStrings *graphicsname
     )
{
  int i,rn;
  int done[300];
  int lfalse=0;
  int ltrue=1;
  int rowtally=0;

  //clean "done" array ready for use
  for (i=0;i<callingseq_length;i++) {
    done[i]=lfalse;
  };

  //print numbers
  if (xml==0) {
    string_out->Add("<table border=\"1\"><tr>\n");
  } else {
    string_out->Add("<callingseq>");
  }


  for (i=0;i<callingseq_length;i++) {
    rn=rand()%callingseq_length;
    while (done[rn]==ltrue) {
      rn=rand()%callingseq_length;
    };

    if (xml==0) {
       //number
      if (selected==1) {
	string_out->Add("<td>");
    string_out->Add(rn);
    string_out->Add("</td>");
      } else if (selected==2) {
      //picture
	string_out->Add("<td><img height=\"");
    string_out->Add(graphic_ysize);
    string_out->Add("\" width=\"");
    string_out->Add(graphic_xsize);
    string_out->Add("\" src=\"");
    string_out->Add(graphicsname->Strings[rn]);
    string_out->Add("\"></td>");
      } else {
        //word
	string_out->Add("<td>");
    string_out->Add(graphicsname->Strings[rn]);
    string_out->Add("</td>");
      };

      rowtally++;
      if (rowtally==callingseq_numbers_per_row) {
	string_out->Add("</tr><tr>");
        rowtally=0;
      };

    } else {
      string_out->Add("<number>");
      string_out->Add(rowtally);
      string_out->Add("</number>");
    };

    done[rn]=ltrue;
  };

  if (xml==0) {
    string_out->Add("</tr></table>\n");
  } else {
    string_out->Add("</callingseq>");
  };

};


void __fastcall TbingoGame::bingofunc (
  int sortrow, //not implemented yet
  int xml, //[0,1]
  int selected, //[1,2,3]  number picture or word
  int num_of_cols_per_card_x,
  int num_of_rows_per_card_y,
  int num_of_cards_x,
  int num_of_cards_y,
  int callingseq_length,     //length of calling sequence .. is this the same as number of chocies?
  int callingseq_numbers_per_row,    //number to print each row (presentation)
  int num_call_seqs,    //number of sets of calling sequences to print
  char *graphics_x_size,
  char *graphics_y_size,
  TStrings *string_out,
  TStrings *graphicsname)

  {

    //doesn't really make sense to have less choices than entries on the card.
    if ((num_of_rows_per_card_y*num_of_cols_per_card_x)>callingseq_length) {
       Application->MessageBox ("There are less boxes on a bingo card than the list of choices. Please increase the list of choices or reduce size of bingo card","Bingo Card Creator",IDOK);
       return;
    };


  //local variable
  int row;

  printheader (xml, string_out);

  //make the sequence unique
  srand (time(0));

  for (row=0;row<num_of_cards_y;row++) {
    printrow (
              xml,
              sortrow,
              num_of_cards_x,
              num_of_cards_y,
              num_of_cols_per_card_x,
              num_of_rows_per_card_y,
              selected,
              graphics_x_size,
              graphics_y_size,
              callingseq_length, //number of choices
              string_out,
              graphicsname
    );
    printrowsep (xml, string_out);
  };


  for (row=0;row<num_call_seqs;row++) {
    printcallingseq (
           xml,
           selected,
           graphics_x_size,
           graphics_y_size,
           callingseq_length,
           callingseq_numbers_per_row,
           string_out,
           graphicsname
     );

    printrowsep (xml, string_out);
  };

  printfooter (xml, string_out);

};

//---------------------------------------------------------------------------





void __fastcall TbingoGame::pictureOrWordToAddKeyPress(TObject *Sender,
      char &Key)
{
      if (Key!='\r')
        return;
      int itemnum,itemfound;
  if (pictureOrWordToAdd->Text=="")
    return;
  itemfound=-1;
  for (itemnum=0;itemnum<ItemList->Items->Count;itemnum++) {
    if (ItemList->Selected[itemnum]) {
      itemfound=itemnum;
      break;
    };
  };

  if (itemfound==-1) {
    ItemList->Items->Append( pictureOrWordToAdd->Text);
  } else {
    ItemList->Items->Insert(itemfound, pictureOrWordToAdd->Text);
  };
  ItemCount->Text=ItemList->Items->Count;
  pictureOrWordToAdd->Text="";

}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------








void __fastcall TbingoGame::ItemListMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   //  AnsiString s=String(Sender->ClassName());
   //  Application->MessageBox(s.c_str(),"mouseup",IDOK);
}
//---------------------------------------------------------------------------

void __fastcall TbingoGame::ItemListEndDrag(TObject *Sender,
      TObject *Target, int X, int Y)
{
  //   Application->MessageBox("dragend","dragend",IDOK);
}
//---------------------------------------------------------------------------

void __fastcall TbingoGame::ItemListDragDrop(TObject *Sender,
      TObject *Source, int X, int Y)
{
  //   Application->MessageBox("dropped","dropped",IDOK);

}
//---------------------------------------------------------------------------

void __fastcall TbingoGame::ItemListDragOver(TObject *Sender,
      TObject *Source, int X, int Y, TDragState State, bool &Accept)
{
 //    Application->MessageBox("dragover","dragover",IDOK);
     Accept=true;
}
//---------------------------------------------------------------------------








void __fastcall TbingoGame::FormResize(TObject *Sender)
{
ItemList->Width=this->Width-30;
int i=this->Height;
i=i-ItemList->Top;
i=i-30;
if (i<30) i=30;
ItemList->Height=i;
}
//---------------------------------------------------------------------------



void __fastcall TbingoGame::ViewOutputClick(TObject *Sender)
{
TRegistry *tr=new TRegistry();
AnsiString appname;
bool result;
HKEY h=HKEY_LOCAL_MACHINE;
tr->RootKey=h;
//note the string that is executed by system call seems to be limited to 127 characters
result=tr->OpenKey("Software\\CLASSES\\htmlfile\\shell\\open\\command",false);
appname=tr->ReadString("");
/* this code removes everything apart from browser name  ie all path info
int rightslash=appname.LastDelimiter("\\");
appname=appname.SubString(rightslash+1,(appname.Length()-rightslash));
//remove any quotes
while (appname.LastDelimiter("\"")>0) {
  appname=appname.SubString(0,appname.LastDelimiter("\"")-1)+
     appname.SubString(appname.LastDelimiter("\"")+1,appname.Length()- appname.LastDelimiter("\""));
}
 but it doesn't work - windows can't find the file without explicit path*/

int sizeofstring=appname.Length()+3+outputFilename->Text.Length();
if (sizeofstring>120) {
  Application->MessageBox("Filepath too long - when added to the filepath for the web browser, there are too many characters to pass too windows. You should put your output in a short filepath for example c:\\work\\out1.htm","Bingo Card Creator",IDOK);
  return ;
}

delete tr;
AnsiString s=appname;
s+=" \""+outputFilename->Text+"\"";
system (s.c_str());
//spawnl(P_NOWAIT, appname.c_str(), appname.c_str(), outputFilename->Text.c_str(), NULL);
}
//---------------------------------------------------------------------------


