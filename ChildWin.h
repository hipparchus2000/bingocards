//----------------------------------------------------------------------------
#ifndef ChildWinH
#define ChildWinH
//----------------------------------------------------------------------------
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <Windows.hpp>
#include <System.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <FileCtrl.hpp>
//----------------------------------------------------------------------------
class TbingoGame : public TForm
{
__published:
    TRadioGroup *bingoGameType;
    TRadioButton *pictureBingo;
    TRadioButton *numberBingo;
    TRadioButton *wordBingo;
    TEdit *outputFilename;
    TLabel *Label2;
    TButton *outputFileButton;
    TListBox *ItemList;
    TEdit *pictureOrWordToAdd;
    TButton *addListItem;
    TButton *removeListItem;
    TLabel *WordOrPictureLabel;
    TLabel *ItemListLabel;
    TUpDown *UpDown1;
    TUpDown *UpDown2;
    TLabel *ItemCountLabel;
    TEdit *ItemCount;
    TLabel *Label6;
    TLabel *Label7;
    TEdit *rowsPerBingoCard;
    TEdit *columnsPerBingoCard;
    TUpDown *UpDown3;
    TUpDown *UpDown4;
    TLabel *Label8;
    TLabel *Label9;
    TEdit *rowsOfCardsInFile;
    TEdit *columnsOfCardsInFile;
    TUpDown *numberBingoMaxUpDown;
    TLabel *numberBingoMaxNumberLabel;
    TEdit *numberBingoMaximumNumber;
    TUpDown *UpDown6;
    TLabel *Label11;
    TEdit *numberOfCallingSequences;
    TUpDown *UpDown7;
    TLabel *Label12;
    TEdit *numberOfCalledItemsPerRow;
    TButton *makeBingoCardsOutputFile;
    TButton *pictureFileButton;
    TOpenDialog *outputfileOpenDialog;
    TOpenDialog *picturefileOpenDialog;
    TPanel *p2;
    TPanel *p3;
    TPanel *p4;
    TButton *RemoveAll;
    TLabel *Label1;
    TPanel *Panel1;
    TButton *ViewOutput;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    
    






    void __fastcall pictureBingoClick(TObject *Sender);

    void __fastcall numberBingoClick(TObject *Sender);
    void __fastcall wordBingoClick(TObject *Sender);

    void __fastcall outputFileButtonClick(TObject *Sender);

    void __fastcall pictureFileButtonClick(TObject *Sender);
    void __fastcall addListItemClick(TObject *Sender);
    void __fastcall removeListItemClick(TObject *Sender);
    void __fastcall removeAllListItemsClick(TObject *Sender);
    void __fastcall makeBingoCardsOutputFileClick(TObject *Sender);


void __fastcall printcard(
      int xml,
      int sortrow,
      int graphicver,
      int num_of_cols_per_card_x,
      int num_of_rows_per_card_y,
      char *graphics_x_size,
      char *graphics_y_size,
      int num_of_choices,
      TStrings *string_out,
      TStrings *graphicsname
    );

void __fastcall printheader (int xml, TStrings *string_out);

void __fastcall printfooter (int xml, TStrings *string_out);

void __fastcall printrow (
              int xml,
              int sortrow,
              int num_of_cards_x,
              int num_of_cards_y,
              int num_of_cols_per_card_x,
              int num_of_rows_per_card_y,
              int graphicver,
              char *graphics_x_size,
              char *graphics_y_size,
              int num_of_choices,
              TStrings *string_out,
              TStrings *graphicsname
              );

void __fastcall printrowsep (int xml, TStrings *string_out);

void __fastcall printcallingseq(
         int xml,
         int graphicver,
    	 char *graphic_xsize,
         char *graphic_ysize,
         int callingseq_length,
         int callingseq_numbers_per_row,
         TStrings *string_out,
         TStrings *graphicsname
     );

void __fastcall bingofunc (
  int sortrow, //not implemented yet
  int xml, //[0,1]
  int graphicver, //[0,1]
  int num_of_cols_per_card_x,
  int num_of_rows_per_card_y,
  int num_of_cards_x,
  int num_of_cards_y,
  int callingseq_length,
  int callingseq_numbers_per_row,
  int num_call_seqs,
  char *graphics_x_size,
  char *graphics_y_size,
  TStrings *string_out,
  TStrings *graphicsname);



    void __fastcall pictureOrWordToAddKeyPress(TObject *Sender, char &Key);

   



    void __fastcall ItemListMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall ItemListEndDrag(TObject *Sender, TObject *Target,
          int X, int Y);
    void __fastcall ItemListDragDrop(TObject *Sender, TObject *Source,
          int X, int Y);
    void __fastcall ItemListDragOver(TObject *Sender, TObject *Source,
          int X, int Y, TDragState State, bool &Accept);
    
    
    
    
    void __fastcall FormResize(TObject *Sender);
    
    
    void __fastcall ViewOutputClick(TObject *Sender);
    
private:
public:
        virtual __fastcall TbingoGame(TComponent *Owner);


}; //end of class
//----------------------------------------------------------------------------
#endif


