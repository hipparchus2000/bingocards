//this program is covered by the GNU Public Licence see www.gnu.org
//for further details
//copyright (or is that left) Jeff Davies feb 2002.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int mod(int inv,int limit) {
	int outv;
	div_t dt=div(inv, limit);
	outv=dt.rem;

	return outv;
};

void printcard(
      int xml,
      int sortrow,
      int graphicver,
      int num_of_cols_per_card_x,
      int num_of_rows_per_card_y,
      char *graphicprefix, 
      char *graphicsuffix, 
      char *graphics_x_size, 
      char *graphics_y_size,
      int num_of_choices
    )

{



	int card[300];
	int x,y,i,rn;
	int done[300];
	int false=0;
	int true=1;

	//clean "done" array ready for use
	for (i=0;i<num_of_choices;i++) {
		done[i]=false;
	};
	
	//fill in numbers
	for (x=0;x<num_of_rows_per_card_y;x++) {
		for (y=0;y<num_of_cols_per_card_x;y++) {
			rn=mod(rand(),num_of_choices);
			while (done[rn]==true) {
				rn=mod(rand(),num_of_choices);
			};
			card[(x*5)+y]=rn;
			done[rn]=true;
		};
	};
	
	//sort rows
	

	//print numbers
	if (xml==0) {
	  printf("<table border=\"1\">");
	} else {
	  printf("<card>\n");
	};

	for (x=0;x<num_of_rows_per_card_y;x++) {
	  if (xml==0) {
	    printf("<tr>\n");
	  } else {
	    printf("<row>\n");
	  };

	  for (y=0;y<num_of_cols_per_card_x;y++) {
	    if (xml==0) {
	      printf("<td>\n");
	    } else {
	      printf("<column>\n");
	    };

	    if (graphicver==0) {
	      printf("%d",card[(x*5)+y]);	      
	    } else {
	      printf("<img height=\"%s\" width=\"%s\" src=\"%s%d.%s\">",graphics_y_size,graphics_x_size,
                  graphicprefix,card[(x*5)+y],graphicsuffix);
	    };  

	    if (xml==0) {
	      printf("</td>\n");
	    } else {
	      printf("</column>\n");
	    };

	  };
	  
	  if (xml==0) {
	    printf("</tr>");
	  } else {
	    printf("</row>\n");
	  };

	};

	if (xml==0) {
	  printf("</table>");
	} else {
	  printf("</card>\n");
	};	
				
};

void printheader (int xml) {

  if (xml==0) {
    printf("<html><head></head><body>");
  } else {
    printf("<bingogames>");
  };

};

void printfooter (int xml) {

  if (xml==0) {
    printf("</body></html>");
  } else {
    printf("</bingogames>");
  };

};


void printrow (
              int xml,
              int sortrow,
              int num_of_cards_x,
              int num_of_cards_y, 
              int num_of_cols_per_card_x,
              int num_of_rows_per_card_y,
              int graphicver, 
              char *graphicprefix, 
              char *graphicsuffix, 
              char *graphics_x_size, 
              char *graphics_y_size,
              int num_of_choices
              )
{

  int col;

  if (xml==0) {
    printf("<table><tr>");
  };

  for (col=0;col<num_of_cards_y;col++) {
    if (xml==0) {
      printf("<td>");
    };

    printcard(
      xml,
      sortrow,
      graphicver,
      num_of_cols_per_card_x,
      num_of_rows_per_card_y,
      graphicprefix, 
      graphicsuffix, 
      graphics_x_size, 
      graphics_y_size,
      num_of_choices
    );

    if (xml==0) {
      printf("</td><td>+</td>");
    };
  };

  if (xml==0) {
    printf("</tr></table>");
  };

}

void printrowsep (int xml) {
  if (xml==0) {
    printf("<p>");
  };
};

void printcallingseq(
         int xml, 
         int graphicver, 
         char *graphicprefix,
         char *graphicsuffix,
	 char *graphic_xsize,
         char *graphic_ysize,
         int callingseq_length,
         int callingseq_numbers_per_row
     )
{
  int i,rn;
  int done[300];
  int false=0;
  int true=1;
  int rowtally=0;

  //clean "done" array ready for use
  for (i=0;i<callingseq_length;i++) {
    done[i]=false;
  };
	
  //print numbers
  if (xml==0) {
    printf("<table border=\"1\"><tr>\n");
  } else {
    printf("<callingseq>");
  }

  
  for (i=0;i<callingseq_length;i++) {
    rn=mod(rand(),callingseq_length);
    while (done[rn]==true) {
      rn=mod(rand(),callingseq_length);
    };
    
    if (xml==0) {
      if (graphicver==0) {
	printf("<td>%d</td>",rn);
      } else {
	printf("<td><img height=\"%s\" width=\"%s\" src=\"%s%d.%s\"></td>",graphic_ysize,graphic_xsize,graphicprefix,rn,graphicsuffix);
      };

      rowtally++;
      if (rowtally==callingseq_numbers_per_row) {
	printf("</tr><tr>");
        rowtally=0;
      };
          
    } else {
      printf("<number>%d</number>");
    };

    done[rn]=true;
  };

  if (xml==0) {
    printf("</tr></table>\n");
  } else {
    printf("</callingseq>");
  };
	
};

void print_command_format() {
  printf("bingo needs either 8 or 12 (graphic mode) additional arguments \n");
  printf("bingo [html|xml] [graphic|number] #cols_per_card #rows per cards #cards_x #cards_y #length_of_calling_sequence #called_numbers_per_line #num_calling_seqs_to_print { graphics_prefix graphics_suffix graphic_height graphic_width } \n");
  printf("\n");
  printf("eg: bingo html number 5 5 4 4 50 25 2 > bingo.htm\n");
  printf("or bingo graphic 5 5 4 4 50 25 2 pic png 10 10\n");

};

int main (int argc, char *argv[]) {

  int xml=0;
  int sortrow=0;
  int graphicver=1;

  int callingseqs=2;
  int row;

  int num_of_cols_per_card_x;
  int num_of_rows_per_card_y;
  int num_of_cards_x;
  int num_of_cards_y;
  int callingseq_length;
  int callingseq_numbers_per_row;
  int num_call_seqs;

  char *graphicprefix;
  char *graphicsuffix;
  char *graphics_x_size;
  char *graphics_y_size;

  //check number of arguments
  if ((argc!=14)&&(argc!=10)) {
    print_command_format();
    printf("number of arguments not 8 or 12 [E1]\n");
    printf("%d arguments given\n",argc);
    return 0;
  };


  //first mandatory argument 
  if (strcmp(argv[1],"xml")==0) {
    xml=1;
  } else {
    if (strcmp(argv[1],"html")==0) {
      xml=0;
    } else {
      print_command_format();
      printf("first argument not 'html' or 'xml' [E2]\n");
      return 0;
    };
  };

  //second mandatory argument
  if (strcmp(argv[2],"graphic")==0) {
    graphicver=1;
  } else {
    if (strcmp(argv[2],"number")==0) {
      graphicver=0;
    } else {
      print_command_format();
      printf("second argument not 'graphic' or 'number' [E3]\n");
      return 0;
    };
  };

  //recheck number of arguments
  if ((graphicver==1) && (argc!=14)) {
    print_command_format();
    printf("graphic mode but 12 arguments not given [E4]\n");
    printf("%d arguments given\n",argc);
    return 0;
  };

  if ((graphicver==0) && (argc!=10)) {
    print_command_format();
    printf("number mode but 8 arguments not given [E5]\n");
    printf("%d arguments given\n",argc);
    return 0;
  };
	
  //num_of_cols_per_card_x   3
  sscanf(argv[3],"%d",&num_of_cols_per_card_x);

  //num_of_rows_per_card_y   4
  sscanf(argv[4],"%d",&num_of_rows_per_card_y);

  //num_of_cards_x           5
  sscanf(argv[5],"%d",&num_of_cards_x);

  //num_of_cards_y           6
  sscanf(argv[6],"%d",&num_of_cards_y);

  //callingseq_length        7
  sscanf(argv[7],"%d",&callingseq_length);

  //callingseq_numbers_per_row  8
  sscanf(argv[8],"%d",&callingseq_numbers_per_row);

  //number of calling sequences to print 9
  sscanf(argv[9],"%d",&num_call_seqs);

  //arguments only for graphics
  if (graphicver==1) {
    graphicprefix=argv[10];
    graphicsuffix=argv[11];
    graphics_x_size=argv[12];
    graphics_y_size=argv[13];
  };

  printheader (xml); 

  //make the sequence unique
  srand (time(0));

  for (row=0;row<num_of_cards_y;row++) {
    printrow (
              xml,
              sortrow,
              num_of_cards_x,
              num_of_cards_y, 
              num_of_cols_per_card_x,
              num_of_rows_per_card_y,
              graphicver, 
              graphicprefix, 
              graphicsuffix, 
              graphics_x_size, 
              graphics_y_size,
              callingseq_length
    );
    printrowsep (xml);
  };


  for (row=0;row<num_call_seqs;row++) {
    printcallingseq (
           xml, 
           graphicver, 
           graphicprefix,
           graphicsuffix,
           graphics_x_size, 
           graphics_y_size,     
           callingseq_length,
           callingseq_numbers_per_row
     );
  
    printrowsep (xml);
  };
  
  printfooter (xml);

};
   





