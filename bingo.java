//this program is covered by the GNU Public Licence see www.gnu.org
//for further details
//copyright (or is that left) Jeff Davies feb 2002.
import java.util.*;
import java.math.*;
import java.io.*;

class bingo {

static Random r;
static String words[]; //words for word bingo
static int numWords;
static int graphicmode=2;
static int wordmode=1;
static int numbermode=0;

static void readWordList(String Filename) {
  words=new String[300];
  try {
    File f=new File(Filename);
    FileInputStream fis=new FileInputStream(f);
    DataInputStream dis=new DataInputStream(fis); 
    int x=0;
    String ins="";
    while (dis.available()>0) {
      words[x]=dis.readLine();
      x++;
    };
    numWords=x;
  } catch (IOException ex) {
    //put standard handler stuff in here
  };

};


static void printcard(
      int xml,
      int sortrow,
      int bingotype,
      int num_of_cols_per_card_x,
      int num_of_rows_per_card_y,
      String graphicprefix, 
      String graphicsuffix, 
      String graphics_x_size, 
      String graphics_y_size,
      int num_of_choices
    )

{



	int card[]; //300 in C version
  	card=new int[300];
	int x,y,i,rn;
	int done[]; //300 in C version
	done=new int[300];
	int falseValue=0;
	int trueValue=1;

	//clean "done" array ready for use
	for (i=0;i<num_of_choices;i++) {
		done[i]=falseValue;
	};
	
	//fill in numbers
	for (x=0;x<num_of_rows_per_card_y;x++) {
		for (y=0;y<num_of_cols_per_card_x;y++) {
			rn=r.nextInt(num_of_choices);
			while (done[rn]==trueValue) {
				rn=r.nextInt(num_of_choices);
			};
			card[(x*num_of_cols_per_card_x)+y]=rn;
			done[rn]=trueValue;
		};
	};
	
	//sort rows
	

	//print numbers
	if (xml==0) {
	  System.out.println("<table border=\"1\">");
	} else {
	  System.out.println("<card>\n");
	};

	for (x=0;x<num_of_rows_per_card_y;x++) {
	  if (xml==0) {
	    System.out.println("<tr>\n");
	  } else {
	    System.out.println("<row>\n");
	  };

	  for (y=0;y<num_of_cols_per_card_x;y++) {
	    if (xml==0) {
	      System.out.println("<td>\n");
	    } else {
	      System.out.println("<column>\n");
	    };

	    if (bingotype==numbermode) {
	      System.out.println(card[(x*num_of_cols_per_card_x)+y]);	      
	    } else if(bingotype==graphicmode) {
	      System.out.println("<img height=\"");
              System.out.print(graphics_y_size);
	      System.out.print("\" width=\"");
              System.out.print(graphics_x_size);
	      System.out.print("\" src=\"");
              System.out.print(graphicprefix);
	      System.out.print(card[(x*num_of_cols_per_card_x)+y]);
	      System.out.print(".");
	      System.out.print(graphicsuffix);
	      System.out.print("\">");
	    } else {
              //word mode bingo
              System.out.println(words[card[(x*num_of_cols_per_card_x)+y]]);
            };  

	    if (xml==0) {
	      System.out.println("</td>\n");
	    } else {
	      System.out.println("</column>\n");
	    };

	  };
	  
	  if (xml==0) {
	    System.out.println("</tr>");
	  } else {
	    System.out.println("</row>\n");
	  };

	};

	if (xml==0) {
	  System.out.println("</table>");
	} else {
	  System.out.println("</card>\n");
	};	
				
};

static void printheader (int xml) {

  if (xml==0) {
    System.out.println("<html><head></head><body>");
  } else {
    System.out.println("<bingogames>");
  };

};

static void printfooter (int xml) {

  if (xml==0) {
    System.out.println("</body></html>");
  } else {
    System.out.println("</bingogames>");
  };

};


static void printrow (
              int xml,
              int sortrow,
              int num_of_cards_x,
              int num_of_cards_y, 
              int num_of_cols_per_card_x,
              int num_of_rows_per_card_y,
              int bingotype, 
              String graphicprefix, 
              String graphicsuffix, 
              String graphics_x_size, 
              String graphics_y_size,
              int num_of_choices
              )
{

  int col;

  if (xml==0) {
    System.out.println("<table><tr>");
  };

  for (col=0;col<num_of_cards_x;col++) {
    if (xml==0) {
      System.out.println("<td>");
    };

    printcard(
      xml,
      sortrow,
      bingotype,
      num_of_cols_per_card_x,
      num_of_rows_per_card_y,
      graphicprefix, 
      graphicsuffix, 
      graphics_x_size, 
      graphics_y_size,
      num_of_choices
    );

    if (xml==0) {
      System.out.println("</td><td>+</td>");
    };
  };

  if (xml==0) {
    System.out.println("</tr></table>");
  };

}

static void printrowsep (int xml) {
  if (xml==0) {
    System.out.println("<p>");
  };
};

static void printcallingseq(
         int xml, 
         int bingotype, 
         String graphicprefix,
         String graphicsuffix,
	 String graphic_xsize,
         String graphic_ysize,
         int callingseq_length,
         int callingseq_numbers_per_row
     )
{
  int i,rn;
  int done[]; //300 in C version
  done=new int[300];
  int falseValue;
  int trueValue;
  int rowtally;

  falseValue=0;
  trueValue=1;
  rowtally=0;

  //clean "done" array ready for use
  for (i=0;i<callingseq_length;i++) {
    done[i]=falseValue;
  };
	
  //print numbers
  if (xml==0) {
    System.out.println("<table border=\"1\"><tr>\n");
  } else {
    System.out.println("<callingseq>");
  }

  
  for (i=0;i<callingseq_length;i++) {
    rn=r.nextInt(callingseq_length);
    while (done[rn]==trueValue) {
      rn=r.nextInt(callingseq_length);
    };
    
    if (xml==0) {
      if (bingotype==numbermode) {
	System.out.println("<td>");
	System.out.println(rn);
	System.out.println("</td>");
      } else if (bingotype==graphicmode) {
	System.out.print("<td><img height=\"");
	System.out.print(graphic_ysize);
	System.out.print("\" width=\"");
	System.out.print(graphic_xsize);
	System.out.print("\" src=\"");
	System.out.print(graphicprefix);
	System.out.print(rn);
	System.out.print(".");
	System.out.print(graphicsuffix);
	System.out.println("\"></td>");
      } else {
        //word mode bingo
        System.out.println("<td>");
        System.out.println(words[rn]);
        System.out.println("</td>");
      };

      rowtally++;
      if (rowtally==callingseq_numbers_per_row) {
	System.out.println("</tr><tr>");
        rowtally=0;
      };
          
    } else {
      System.out.println("<number>%d</number>");
    };

    done[rn]=trueValue;
  };

  if (xml==0) {
    System.out.println("</tr></table>\n");
  } else {
    System.out.println("</callingseq>");
  };
	
};

static void print_command_format(String argv[]) {
  System.out.println("bingo needs either 8 or 12 (graphic mode) additional arguments \n");
  System.out.println("bingo [html|xml] [graphic|number|word] #cols_per_card #rows per cards #cards_x #cards_y [#length_of_calling_sequence|filename_of_word_list] #called_numbers_per_line #num_calling_seqs_to_print { graphics_prefix graphics_suffix graphic_height graphic_width } \n");
  System.out.println("\n");
  System.out.println("eg: bingo html number 5 5 4 4 50 25 2 > bingo.htm\n");
  System.out.println("or bingo graphic 5 5 4 4 50 25 2 pic png 10 10\n");
  System.out.println("for word bingo replace length_of_calling_sequence with filename containing words");

  int i;
  System.out.println("Argument Dump");
  for (i=0;i<argv.length;i++) {
    System.out.print(i);
    System.out.print(" = ");
    System.out.println(argv[i]);
  };

};

public static void main (String argv[]) {

  int argc;
  argc=argv.length;

  int xml;
  xml=0;
  int sortrow;
  sortrow=0;
  int bingotype;
  bingotype=graphicmode;

  int callingseqs;
  callingseqs=2;
  int row;

  int num_of_cols_per_card_x;
  int num_of_rows_per_card_y;
  int num_of_cards_x;
  int num_of_cards_y;
  int callingseq_length;
  int callingseq_numbers_per_row;
  int num_call_seqs;

  String graphicprefix;
  graphicprefix="pic";

  String graphicsuffix;
  graphicsuffix="png";

  String graphics_x_size;
  graphics_x_size="20";

  String graphics_y_size;
  graphics_y_size="20";

  //check number of arguments
  if ((argc!=13)&&(argc!=9)) {
    print_command_format(argv);
    System.out.println("number of arguments not 13 or 9 [E1]");
    System.out.print(argc);
    System.out.println(" arguments given");
    return;
  };


  //first mandatory argument 
  if (argv[0].compareTo("xml")==0) {
    xml=1;
  } else {
    if (argv[0].compareTo("html")==0) {
      xml=0;
    } else {
      print_command_format(argv);
      System.out.println("first argument not 'html' or 'xml' [E2]");
      return;
    };
  };

  //second mandatory argument
  if (argv[1].compareTo("graphic")==0) {
    bingotype=graphicmode;
  } else {
    if (argv[1].compareTo("number")==0) {
      bingotype=numbermode;
    } else {
      if (argv[1].compareTo("word")==0) {
        bingotype=wordmode; 
      } else {
        print_command_format(argv);
        System.out.println("second argument not 'graphic' or 'number' [E3]");
        return;
      };
    };
  };


  //recheck number of arguments
  if ((bingotype==graphicmode) && (argc!=13)) {
    print_command_format(argv);
    System.out.println("graphic mode but 13 arguments not given [E4]");
    System.out.println(argc);
    System.out.println(" arguments given");
    return;
  };

  if ((bingotype==numbermode) && (argc!=9)) {
    print_command_format(argv);
    System.out.println("number mode but 9 arguments not given [E5]");
    System.out.print(argc);
    System.out.println(" arguments given");
    return;
  };
	
  //num_of_cols_per_card_x   3
  num_of_cols_per_card_x=Integer.parseInt(argv[2]);

  //num_of_rows_per_card_y   4
  num_of_rows_per_card_y=Integer.parseInt(argv[3]);

  //num_of_cards_x           5
  num_of_cards_x=Integer.parseInt(argv[4]);

  //num_of_cards_y           6
  num_of_cards_y=Integer.parseInt(argv[5]);

  if (bingotype==wordmode) {
    readWordList(argv[6]);
    callingseq_length=numWords;
  } else {
    //callingseq_length        7
    callingseq_length=Integer.parseInt(argv[6]);
  };

  //callingseq_numbers_per_row  8
  callingseq_numbers_per_row=Integer.parseInt(argv[7]);

  //number of calling sequences to print 9
  num_call_seqs=Integer.parseInt(argv[8]);

  //arguments only for graphics
  if (bingotype==graphicmode) {
    graphicprefix=argv[9];
    graphicsuffix=argv[10];
    graphics_x_size=argv[11];
    graphics_y_size=argv[12];
  };

  printheader (xml); 

  //make the sequence unique
  r=new Random();

  //srand (time(0));

  for (row=0;row<num_of_cards_y;row++) {
    printrow (
              xml,
              sortrow,
              num_of_cards_x,
              num_of_cards_y, 
              num_of_cols_per_card_x,
              num_of_rows_per_card_y,
              bingotype, 
              graphicprefix, 
              graphicsuffix, 
              graphics_x_size, 
              graphics_y_size,
              callingseq_length
    );
    printrowsep (xml);
  };


  for (row=0;row<num_call_seqs;row++) {
    printcallingseq (
           xml, 
           bingotype, 
           graphicprefix,
           graphicsuffix,
           graphics_x_size, 
           graphics_y_size,     
           callingseq_length,
           callingseq_numbers_per_row
     );
  
    printrowsep (xml);
  };
  
  printfooter (xml);

};
   

} //end class



