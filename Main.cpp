//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "About.h"
//---------------------------------------------------------------------
#pragma resource "*.dfm"
TMainForm *MainForm;
/*
  Bingo Card Creator version 1.01.
Copyright (C) 2003 Jeff Davies  (jeff@llandre.freeserve.co.uk)
address: Pencoed, Cnwch Coch, Trawscoed, Aberystwyth, Ceredigion, SY23 4RR, UK.

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

*/


/* the new message handler (an attempt to catch drag drop, couldn't
make it work, so it's commented out for now)

LRESULT CALLBACK NewWindowProc(HWND hWnd,UINT msg,WPARAM w, LPARAM l) {

  //this works ok!
  if (msg == WM_RBUTTONDOWN) {
     Application->MessageBox("right mouse button","clicked",IDOK);
  };

  if (msg == WM_QUERYDRAGICON) {
     Application->MessageBox("query drag icon","clicked",IDOK);
  };

  if (msg == WM_DROPFILES) {
     Application->MessageBox("formdragdrop","dropped",IDOK);
  };

  //too many messages so this is commented out
  //if (msg!=70) {

  //  TVarRec v[] = { (int)msg };
  //	ShowMessage(Format("%d", v ,1));

  //  };

 return CallWindowProc((FARPROC)MainForm->OldWindowProc, hWnd,msg,w,l);

};
*/

//---------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent *Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------
void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	Application->OnHint = ShowHint;
	Screen->OnActiveFormChange = UpdateMenuItems;

    // attempt to catch dragdrop operations. Failed.
     //    OldWindowProc = (WNDPROC)SetWindowLong(this->Handle,GWL_WNDPROC,
     //     (long)NewWindowProc);
     //DragAcceptFiles(this->Handle,true);
}

//---------------------------------------------------------------------
void __fastcall TMainForm::ShowHint(TObject *Sender)
{
	StatusBar->SimpleText = Application->Hint;
}
//---------------------------------------------------------------------
void __fastcall TMainForm::CreateMDIChild(String Name)
{
	TbingoGame *Child;

	//--- create a new MDI child window ----
	Child = new TbingoGame(Application);
	Child->Caption = Name;
    Child->Height=580;
    Child->Width=880;
}
//---------------------------------------------------------------------
void __fastcall TMainForm::FileNewItemClick(TObject *Sender)
{
	CreateMDIChild("NONAME" + IntToStr(MDIChildCount + 1)+".BGO");
}
//---------------------------------------------------------------------
void __fastcall TMainForm::FileOpenItemClick(TObject *Sender)
{
	if (OpenDialog->Execute())
		CreateMDIChild(OpenDialog->FileName);
    TbingoGame* bg=(TbingoGame *)ActiveMDIChild;
    AnsiString as=OpenDialog->FileName;
    if (as.AnsiCompare("")==0) return ;
    bg->Caption=OpenDialog->FileName;
/*    int selected=bg->bingoGameType->ItemIndex; //number, picture, word
    if (selected==1) {
      bg->ItemList->Items->Insert(0,"Number");
    }
    if (selected==2) {
      bg->ItemList->Items->Insert(0,"Picture");
    }
    if (selected==3) {
      bg->ItemList->Items->Insert(0,"Word");
    }; */
    bg->ItemList->Items->LoadFromFile(bg->Caption);
    AnsiString type=bg->ItemList->Items->Strings[0];
    if (type=="Number") {
      bg->numberBingo->Checked=true;
      bg->pictureBingo->Checked=false;
      bg->wordBingo->Checked=false;
    };
    if (type=="Picture") {
      bg->numberBingo->Checked=false;
      bg->pictureBingo->Checked=true;
      bg->wordBingo->Checked=false;
    };
    if (type=="Word") {
      bg->numberBingo->Checked=false;
      bg->pictureBingo->Checked=false;
      bg->wordBingo->Checked=true;
    };
    bg->ItemList->Items->Delete(0);

    bg->numberBingoMaximumNumber->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    bg->rowsPerBingoCard->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    bg->columnsPerBingoCard->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    bg->rowsOfCardsInFile->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    bg->columnsOfCardsInFile->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    bg->numberOfCallingSequences->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    bg->numberOfCalledItemsPerRow->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    bg->outputFilename->Text=bg->ItemList->Items->Strings[0];
    bg->ItemList->Items->Delete(0);

    //update the list count
    bg->ItemCount->Text=bg->ItemList->Items->Count;

}
//---------------------------------------------------------------------
void __fastcall TMainForm::FileCloseItemClick(TObject *Sender)
{
	if (ActiveMDIChild)
		ActiveMDIChild->Close();
}
//---------------------------------------------------------------------
void __fastcall TMainForm::FileSaveItemClick(TObject *Sender)
{
	//---- save current file (ActiveMDIChild points to the window) ----
    TbingoGame* bg=(TbingoGame *)ActiveMDIChild;
    AnsiString cap=bg->Caption;

    //if the caption starts with Noname, then file save as
    if (cap.SubString(1,6)==AnsiString("NONAME")) {
      SaveDialog1->FileName=ActiveMDIChild->Caption;
      if (SaveDialog1->Execute()) ActiveMDIChild->Caption=SaveDialog1->FileName;
    };



    int selected;
    if (bg->numberBingo->Checked==true) selected=1; //number, picture, word
    if (bg->pictureBingo->Checked==true) selected=2;
    if (bg->wordBingo->Checked==true) selected=3;

    //Number
    if (selected==1) {
      bg->ItemList->Items->Insert(0,bg->outputFilename->Text);
      bg->ItemList->Items->Insert(0,bg->numberOfCalledItemsPerRow->Text);
      bg->ItemList->Items->Insert(0,bg->numberOfCallingSequences->Text);
      bg->ItemList->Items->Insert(0,bg->columnsOfCardsInFile->Text);
      bg->ItemList->Items->Insert(0,bg->rowsOfCardsInFile->Text);
      bg->ItemList->Items->Insert(0,bg->columnsPerBingoCard->Text);
      bg->ItemList->Items->Insert(0,bg->rowsPerBingoCard->Text);
      bg->ItemList->Items->Insert(0,bg->numberBingoMaximumNumber->Text);
      bg->ItemList->Items->Insert(0,"Number");

      bg->ItemList->Items->SaveToFile(cap);

        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
    };

    //Picture
    if (selected==2) {
      bg->ItemList->Items->Insert(0,bg->outputFilename->Text);
      bg->ItemList->Items->Insert(0,bg->numberOfCalledItemsPerRow->Text);
      bg->ItemList->Items->Insert(0,bg->numberOfCallingSequences->Text);
      bg->ItemList->Items->Insert(0,bg->columnsOfCardsInFile->Text);
      bg->ItemList->Items->Insert(0,bg->rowsOfCardsInFile->Text);
      bg->ItemList->Items->Insert(0,bg->columnsPerBingoCard->Text);
      bg->ItemList->Items->Insert(0,bg->rowsPerBingoCard->Text);
      bg->ItemList->Items->Insert(0,bg->numberBingoMaximumNumber->Text);
      bg->ItemList->Items->Insert(0,"Picture");

      bg->ItemList->Items->SaveToFile(cap);

        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
    };

    //Words
    if (selected==3) {
      bg->ItemList->Items->Insert(0,bg->outputFilename->Text);
      bg->ItemList->Items->Insert(0,bg->numberOfCalledItemsPerRow->Text);
      bg->ItemList->Items->Insert(0,bg->numberOfCallingSequences->Text);
      bg->ItemList->Items->Insert(0,bg->columnsOfCardsInFile->Text);
      bg->ItemList->Items->Insert(0,bg->rowsOfCardsInFile->Text);
      bg->ItemList->Items->Insert(0,bg->columnsPerBingoCard->Text);
      bg->ItemList->Items->Insert(0,bg->rowsPerBingoCard->Text);
      bg->ItemList->Items->Insert(0,bg->numberBingoMaximumNumber->Text);
      bg->ItemList->Items->Insert(0,"Word");

      bg->ItemList->Items->SaveToFile(cap);

        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
        bg->ItemList->Items->Delete(0);
    };


}
//---------------------------------------------------------------------
void __fastcall TMainForm::FileSaveAsItemClick(TObject *Sender)
{
	//---- save current file under new name ----
    SaveDialog1->FileName=ActiveMDIChild->Caption;
    if (SaveDialog1->Execute()) ActiveMDIChild->Caption=SaveDialog1->FileName;
    FileSaveItemClick(Sender);
}
//---------------------------------------------------------------------
void __fastcall TMainForm::FileExitItemClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------
void __fastcall TMainForm::CutItemClick(TObject *Sender)
{
	//---- cut selection to clipboard ----
}
//--------------------------------------------------------------------- 
void __fastcall TMainForm::CopyItemClick(TObject *Sender)
{
	//---- copy selection to clipboard ----
}
//--------------------------------------------------------------------- 
void __fastcall TMainForm::PasteItemClick(TObject *Sender)
{
	//---- paste from clipboard ----
}
//--------------------------------------------------------------------- 
void __fastcall TMainForm::WindowCascadeItemClick(TObject *Sender)
{
	Cascade();
}
//--------------------------------------------------------------------- 
void __fastcall TMainForm::WindowTileItemClick(TObject *Sender)
{
	Tile();
}
//--------------------------------------------------------------------- 
void __fastcall TMainForm::WindowArrangeItemClick(TObject *Sender)
{
	ArrangeIcons();
}
//--------------------------------------------------------------------- 
void __fastcall TMainForm::WindowMinimizeItemClick(TObject *Sender)
{
	int i;

	//---- Must be done backwards through the MDIChildren array ----
	for (i=MDIChildCount-1; i >= 0; i--)
		MDIChildren[i]->WindowState = wsMinimized;
}
//---------------------------------------------------------------------
void __fastcall TMainForm::UpdateMenuItems(TObject *Sender)
{
	FileCloseItem->Enabled = MDIChildCount > 0;
	FileSaveItem->Enabled = MDIChildCount > 0;
	FileSaveAsItem->Enabled = MDIChildCount > 0;
	CutItem->Enabled = MDIChildCount > 0;
	CopyItem->Enabled = MDIChildCount > 0;
	PasteItem->Enabled = MDIChildCount > 0;
	SaveBtn->Enabled = MDIChildCount > 0;
	CutBtn->Enabled = MDIChildCount > 0;
	CopyBtn->Enabled = MDIChildCount > 0;
	PasteBtn->Enabled = MDIChildCount > 0;
	WindowCascadeItem->Enabled = MDIChildCount > 0;
	WindowTileItem->Enabled = MDIChildCount > 0;
	WindowArrangeItem->Enabled = MDIChildCount > 0;
	WindowMinimizeItem->Enabled = MDIChildCount > 0;
}
//---------------------------------------------------------------------
void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
	Screen->OnActiveFormChange = NULL;
    //returns the pointer to window message handler to default.
    //This is to undo the change made in FormCreate (an attempt
    //to catch Window Messages related to DragDrop.
//      SetWindowLong(this->Handle, GWL_WNDPROC, (long)OldWindowProc);

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::HelpAboutItemClick(TObject *Sender)
{
    AboutBox->ShowModal();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------



void __fastcall TMainForm::FormDragOver(TObject *Sender, TObject *Source,
      int X, int Y, TDragState State, bool &Accept)
{
Accept=true;
}
//---------------------------------------------------------------------------


